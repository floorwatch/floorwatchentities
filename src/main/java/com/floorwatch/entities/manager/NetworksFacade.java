package com.floorwatch.entities.manager;

import com.floorwatch.entities.Networks;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author DaleDavis
 */
public class NetworksFacade extends FacadeBase {
    
    private Logger log = Logger.getLogger(NetworksFacade.class);      
    
    /** Creates a new instance of NetworksFacade */
    public NetworksFacade() {
    }
    
    @SuppressWarnings("unchecked")
    public Networks findById(Integer id){
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Networks.findById");
            q.setParameter("id", id);
            return (Networks)q.getSingleResult();
        } catch(NoResultException nre) {
            throw nre;            
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    public Networks findByDescription(String description){
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Networks.findByDescription");
            q.setParameter("description", description);
            return (Networks)q.getSingleResult();
        } catch(NoResultException nre) {
            throw nre;            
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }    
}
