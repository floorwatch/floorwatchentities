package com.floorwatch.entities.manager;

import com.floorwatch.entities.Reports;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author DaleDavis
 */
public class ReportsFacade extends FacadeBase {

    private Logger log = Logger.getLogger(ReportsFacade.class);

    /**
     * Creates a new instance of ReportsFacade
     */
    public ReportsFacade() {
    }

    @SuppressWarnings("unchecked")
    public Reports findByName(String name) {
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Reports.findByName");
            q.setParameter("name", name);
            return (Reports) q.getSingleResult();
        } catch (NoResultException nre) {
            throw nre;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public Reports findById(Integer id) {
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Reports.findById");
            q.setParameter("id", id);
            return (Reports) q.getSingleResult();
        } catch (NoResultException nre) {
            throw nre;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }    

    @SuppressWarnings("unchecked")
    public List<Reports> findAll() {
        log.info("Facade Method: ReportsFacade.findAll");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Reports> list = new ArrayList<Reports>();
            Query q = em.createNamedQuery("Reports.findAll");
            List<Reports> results = q.getResultList();
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");
            if (results != null) {
                list.addAll(results);
            }
            return list;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public Reports save(Reports company) {
        EntityManager em = EntityManagerHelper.createEntityManager();
        try {
            em.getTransaction().begin();
            company = em.merge(company);
            em.getTransaction().commit();
            return company;
        } catch (Exception e) {
            try {
                em.getTransaction().rollback();
            } catch (Exception ex) {
            }
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

}
