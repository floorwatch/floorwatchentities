package com.floorwatch.entities.manager;

import com.floorwatch.entities.Stores;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author DaleDavis
 */
public class StoresFacade extends FacadeBase {
    
    private Logger log = Logger.getLogger(StoresFacade.class);      
    
    /** Creates a new instance of StoresFacade */
    public StoresFacade() {
    }
    
    @SuppressWarnings("unchecked")
    public Stores findById(Integer id){
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Stores.findById");
            q.setParameter("id", id);
            return (Stores)q.getSingleResult();
        } catch(NoResultException nre) {
            throw nre;            
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Stores> findAll(){
        log.info("Facade Method: StoresFacade.findAll");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Stores> list = new ArrayList<Stores>();
            Query q = em.createNamedQuery("Stores.findAll");
            List<Stores> results =  q.getResultList();
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            if(results != null){
                list.addAll(results);
            }
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Stores> findByCompanyId(Integer companyId){
        log.info("Facade Method: StoresFacade.findByCompanyId");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Stores> list = new ArrayList<Stores>();
            String sql = "select s from Stores s "
                    + "where s.companyId.id = :companyId "
                    + "order by s.description";            
            Query q = em.createQuery(sql);
            q.setParameter("companyId", companyId);
            List<Stores> results =  q.getResultList();
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            if(results != null){
                list.addAll(results);
            }
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }    
    
    @SuppressWarnings("unchecked")
    public List<Stores> findByCity(String city, String state){
        log.info("Facade Method: StoresFacade.findByCity");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Stores> list = new ArrayList<Stores>();
            String sql = "select s from Stores s "
                    + "where s.city = :city "
                    + "and s.state = :state";
            Query q = em.createQuery(sql);
            q.setParameter("city", city);
            q.setParameter("state", state);
            List<Stores> results =  q.getResultList();
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            if(results != null){
                list.addAll(results);
            }
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }     

    @SuppressWarnings("unchecked")
    public List<Stores> findNearest(Double lat, Double lon, Double distance){
        log.info("Facade Method: StoresFacade.findNearest");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Stores> list = new ArrayList<Stores>();
            String sql = "SELECT id, company_id, description, address_1, address_2, city, state, country, postal_code, phone, latitude, longitude, created_by, created_at, updated_by, updated_at, distance "
                    + "FROM ("
                    + "SELECT s.id, s.company_id, s.description, s.address_1, s.address_2, s.city, s.state, s.country, s.postal_code, s.phone, "
                    + "s.latitude, s.longitude, s.created_by, s.created_at, s.updated_by, s.updated_at, "
                    + "p.radius, p.distance_unit " 
                    + "* DEGREES(ACOS(COS(RADIANS(p.latpoint)) "
                    + "* COS(RADIANS(s.latitude)) "
                    + "* COS(RADIANS(p.longpoint - s.longitude)) "
                    + "+ SIN(RADIANS(p.latpoint)) "
                    + "* SIN(RADIANS(s.latitude)))) AS distance "
                    + "FROM stores AS s "
                    + "JOIN ("
                    + "SELECT ? AS latpoint, ? AS longpoint, "
                    + "? AS radius, 69.0 AS distance_unit) AS p ON 1 = 1 "
                    + "WHERE s.latitude "
                    + "BETWEEN p.latpoint  - (p.radius / p.distance_unit) "
                    + "AND p.latpoint  + (p.radius / p.distance_unit) "
                    + "AND s.longitude "
                    + "BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint)))) "
                    + "AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint)))) "
                    + ") AS d "
                    + "WHERE distance <= radius "
                    + "ORDER BY distance";
            
            Query q = em.createNativeQuery(sql, "StoresWithDistance");
            q.setParameter(1, lat.doubleValue());
            q.setParameter(2, lon.doubleValue());
            q.setParameter(3, distance.doubleValue());
            List<Object[]> results =  q.getResultList();
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            if(results != null){
                for (Object[] result : results) {
                    Stores store = (Stores)result[0];
                    Double distanceResult = (Double)result[1];
                    store.setDistance(distanceResult);
                    list.add(store);
                }    
            }
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    public Stores save(Stores store) {
        EntityManager em = EntityManagerHelper.createEntityManager();
        try {
            if (store.getId() != null) {
                em.getTransaction().begin();
                store = em.merge(store);
                em.getTransaction().commit();
            } else {
                em.getTransaction().begin();
                em.persist(store);
                em.getTransaction().commit();
            }
            return store;
        } catch(Exception e) {
            try {
                em.getTransaction().rollback();
            } catch(Exception ex) {}
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }

}
