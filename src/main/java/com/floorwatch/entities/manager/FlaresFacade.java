package com.floorwatch.entities.manager;

import com.floorwatch.entities.Flares;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author DaleDavis
 */
public class FlaresFacade extends FacadeBase {
    
    private Logger log = Logger.getLogger(FlaresFacade.class);      
    
    /** Creates a new instance of FlaresFacade */
    public FlaresFacade() {
    }
    
    @SuppressWarnings("unchecked")
    public Flares findById(Integer id){
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Flares.findById");
            q.setParameter("id", id);
            return (Flares)q.getSingleResult();
        } catch(NoResultException nre) {
            throw nre;            
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public Flares findByTimeUser(Date createdAt, String username){
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            
            String sql = "select f from Flares f "
                    + "where f.customerUserId.username = :username "
                    + "and f.createdAt = :createdAt";
            
            Query q = em.createQuery(sql);
            q.setParameter("username", username);
            q.setParameter("createdAt", createdAt);
            return (Flares)q.getResultList().get(0);
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }    
    
    @SuppressWarnings("unchecked")
    public List<Flares> findUnresolvedByCompany(Integer companyId){
        log.info("Facade Method: FlaresFacade.findUnresolvedByCompany");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "where f.storeId.companyId.id = :companyId "
                    + "and f.resolved = 0 "
                    + "order by f.createdAt";
            Query q = em.createQuery(sql);
            q.setParameter("companyId", companyId);
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Flares> findAllUnresolved(){
        log.info("Facade Method: FlaresFacade.findAllUnresolved");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "where f.resolved = 0 "
                    + "order by f.createdAt";
            Query q = em.createQuery(sql);
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }    
    
    @SuppressWarnings("unchecked")
    public List<Flares> findResolvedByStore(Integer storeId){
        log.info("Facade Method: FlaresFacade.findResolvedByStore");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "where f.storeId.id = :storeId "
                    + "and f.resolved = 1 "
                    + "order by f.resolvedAt";
            Query q = em.createQuery(sql);
            q.setParameter("storeId", storeId);
            Calendar now = Calendar.getInstance();
            now.set(Calendar.HOUR, 0);
            now.set(Calendar.MINUTE, 0);
            now.set(Calendar.SECOND, 0);
            now.set(Calendar.MILLISECOND, 0);
            q.setParameter("resolvedMidnight", now.getTime());
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Flares> findResolvedByStoreMidnight(Integer storeId){
        log.info("Facade Method: FlaresFacade.findResolvedByStore");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "where f.storeId.id = :storeId "
                    + "and f.resolved = 1 "
                    + "and f.resolvedAt > :resolvedMidnight "
                    + "order by f.resolvedAt";
            Query q = em.createQuery(sql);
            q.setParameter("storeId", storeId);
            Calendar now = Calendar.getInstance();
            now.set(Calendar.HOUR, 0);
            now.set(Calendar.MINUTE, 0);
            now.set(Calendar.SECOND, 0);
            now.set(Calendar.MILLISECOND, 0);
            q.setParameter("resolvedMidnight", now.getTime());
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }    
    
    @SuppressWarnings("unchecked")
    public List<Flares> findUnresolvedByStore(Integer storeId){
        log.info("Facade Method: FlaresFacade.findUnresolvedByStore");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "where f.storeId.id = :storeId "
                    + "and f.resolved = 0 "
                    + "order by f.createdAt";
            Query q = em.createQuery(sql);
            q.setParameter("storeId", storeId);
            Calendar now = Calendar.getInstance();
            now.set(Calendar.HOUR, 0);
            now.set(Calendar.MINUTE, 0);
            now.set(Calendar.SECOND, 0);
            now.set(Calendar.MILLISECOND, 0);
            q.setParameter("createdMidnight", now.getTime());            
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Flares> findUnresolvedByStoreMidnight(Integer storeId){
        log.info("Facade Method: FlaresFacade.findUnresolvedByStore");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "where f.storeId.id = :storeId "
                    + "and f.resolved = 0 "
                    + "and f.createdAt > :createdMidnight "
                    + "order by f.createdAt";
            Query q = em.createQuery(sql);
            q.setParameter("storeId", storeId);
            Calendar now = Calendar.getInstance();
            now.set(Calendar.HOUR, 0);
            now.set(Calendar.MINUTE, 0);
            now.set(Calendar.SECOND, 0);
            now.set(Calendar.MILLISECOND, 0);
            q.setParameter("createdMidnight", now.getTime());            
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }    
    
    @SuppressWarnings("unchecked")
    public List<Flares> findResolvedByCompany(Integer companyId){
        log.info("Facade Method: FlaresFacade.findResolvedByCompany");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "where f.storeId.companyId.id = :companyId "
                    + "and f.resolved = 1 "
                    + "order by f.resolvedAt";
            Query q = em.createQuery(sql);
            q.setParameter("companyId", companyId);
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Flares> findAllResolved(){
        log.info("Facade Method: FlaresFacade.findAllResolved");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "where f.resolved = 1 "
                    + "order by f.resolvedAt";
            Query q = em.createQuery(sql);
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Flares> findAllByCompany(Integer companyId){
        log.info("Facade Method: FlaresFacade.findAllByCompany");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "where f.storeId.companyId.id = :companyId "
                    + "order by f.resolvedAt";
            Query q = em.createQuery(sql);
            q.setParameter("companyId", companyId);
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Flares> findAll(){
        log.info("Facade Method: FlaresFacade.findAll");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select f from Flares f "
                    + "order by f.resolvedAt";
            Query q = em.createQuery(sql);
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Flares> findActiveUnresolvedByStore(Integer storeId){
        log.info("Facade Method: FlaresFacade.findActiveUnresolvedByStore");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Flares> list = new ArrayList<Flares>();
            String sql = "select * from flares "
                    + "where store_id = ? "
                    + "and resolved = 0 "
                    + "and created_at >= date_sub(now() , interval 5 minute) "
                    + "order by created_at";
            Query q = em.createNativeQuery(sql, Flares.class);
            q.setParameter(1, storeId);
            List<Flares> results = q.getResultList();
            if (results != null) {
                list.addAll(results);
            }
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }     

    public Flares save(Flares store) {
        EntityManager em = EntityManagerHelper.createEntityManager();
        try {
            em.getTransaction().begin();
            store = em.merge(store);
            em.getTransaction().commit();
            return store;
        } catch (Exception e) {
            try {
                em.getTransaction().rollback();
            } catch (Exception ex) {
            }
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

}
