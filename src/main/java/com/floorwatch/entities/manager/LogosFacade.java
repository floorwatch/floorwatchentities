package com.floorwatch.entities.manager;

import com.floorwatch.entities.Logos;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author DaleDavis
 */
public class LogosFacade extends FacadeBase {

    private Logger log = Logger.getLogger(LogosFacade.class);

    /**
     * Creates a new instance of LogosFacade
     */
    public LogosFacade() {
    }

    @SuppressWarnings("unchecked")
    public Logos findById(Integer id) {
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Logos.findById");
            q.setParameter("id", id);
            return (Logos) q.getSingleResult();
        } catch (NoResultException nre) {
            throw nre;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public Logos save(Logos logo) {
        EntityManager em = EntityManagerHelper.createEntityManager();
        try {
            em.getTransaction().begin();
            logo = em.merge(logo);
            em.getTransaction().commit();
            return logo;
        } catch (Exception e) {
            try {
                em.getTransaction().rollback();
            } catch (Exception ex) {
            }
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

}
