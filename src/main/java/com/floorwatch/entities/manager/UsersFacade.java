package com.floorwatch.entities.manager;

import com.floorwatch.entities.UserStores;
import com.floorwatch.entities.Users;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author DaleDavis
 */
public class UsersFacade extends FacadeBase {

    private Logger log = Logger.getLogger(UsersFacade.class);

    /**
     * Creates a new instance of UsersFacade
     */
    public UsersFacade() {
    }

    @SuppressWarnings("unchecked")
    public Users findById(Integer id) {
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Users.findById");
            q.setParameter("id", id);
            return (Users) q.getSingleResult();
        } catch (NoResultException nre) {
            throw nre;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public Users findByUsername(String username) {
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Users.findByUsername");
            q.setParameter("username", username);
            return (Users) q.getSingleResult();
        } catch (NoResultException nre) {
            throw nre;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }
    
    public List<Users> findOnDutyByStore(Integer storeId) {
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            List<Users> list = new ArrayList<Users>();
            String sql = "select us from UserStores us "
                    + "where us.storeId.id = :storeId "
                    + "and us.onDuty = 1";
            Query q = em.createQuery(sql);
            q.setParameter("storeId", storeId);
            List<UserStores> results = q.getResultList();
            Calendar endCal = Calendar.getInstance();
            if (results != null) {
                for (UserStores userStore : results) {
                    list.add(userStore.getUserId());
                }    
            }
            return list;
        } catch (NoResultException nre) {
            throw nre;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }    

    @SuppressWarnings("unchecked")
    public List<Users> findAll() {
        log.info("Facade Method: UsersFacade.findAll");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Users> list = new ArrayList<Users>();
            Query q = em.createNamedQuery("Users.findAll");
            List<Users> results = q.getResultList();
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");
            if (results != null) {
                list.addAll(results);
            }
            return list;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public Users save(Users user) {
        EntityManager em = EntityManagerHelper.createEntityManager();
        try {
            em.getTransaction().begin();
            user = em.merge(user);
            em.getTransaction().commit();
            return user;
        } catch (Exception e) {
            try {
                em.getTransaction().rollback();
            } catch (Exception ex) {
            }
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

}
