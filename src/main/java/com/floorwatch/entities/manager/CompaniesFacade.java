package com.floorwatch.entities.manager;

import com.floorwatch.entities.Companies;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author DaleDavis
 */
public class CompaniesFacade extends FacadeBase {

    private Logger log = Logger.getLogger(CompaniesFacade.class);

    /**
     * Creates a new instance of CompaniesFacade
     */
    public CompaniesFacade() {
    }

    @SuppressWarnings("unchecked")
    public Companies findById(Integer id) {
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Companies.findById");
            q.setParameter("id", id);
            return (Companies) q.getSingleResult();
        } catch (NoResultException nre) {
            throw nre;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public Companies findByDescription(String description) {
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Companies.findByDescription");
            q.setParameter("description", description);
            return (Companies) q.getSingleResult();
        } catch (NoResultException nre) {
            throw nre;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Companies> findAll() {
        log.info("Facade Method: CompaniesFacade.findAll");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Companies> list = new ArrayList<Companies>();
            Query q = em.createNamedQuery("Companies.findAll");
            List<Companies> results = q.getResultList();
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");
            if (results != null) {
                list.addAll(results);
            }
            return list;
        } catch (Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public Companies save(Companies company) {
        EntityManager em = EntityManagerHelper.createEntityManager();
        try {
            em.getTransaction().begin();
            company = em.merge(company);
            em.getTransaction().commit();
            return company;
        } catch (Exception e) {
            try {
                em.getTransaction().rollback();
            } catch (Exception ex) {
            }
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

}
