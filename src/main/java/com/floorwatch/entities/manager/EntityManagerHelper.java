package com.floorwatch.entities.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.apache.log4j.Logger;

public class EntityManagerHelper {

    private static String PROPERTIES_FILENAME = "/floorwatch.properties";
    public static final String PERSISTENCE_UNIT_NAME = "FloorWatchEntitiesPU";
    private static Logger log = Logger.getLogger(EntityManagerHelper.class);
    private static Properties properties = null;
    private static EntityManagerFactory emf;

    private static class EntityManagerHelperHolder {
        private static final EntityManagerHelper INSTANCE = new EntityManagerHelper();
    }

    /**
     *
     * Creates a new instance of EntityManagerHelper
     */
    private EntityManagerHelper() {
    }

    // Pass in a # delimited string and parse them into a String array
    // where string[0] is the property name and string[1] is the value
    public static String[] getNameValuePair(String string) {
        return string.split("#");
    }

    private HashMap createProperties() {
        log.info("Loading JPA properties");
        HashMap props = new HashMap();
        String urlProp = "db.url";
        String userProp = "db.username";
        String passwordProp = "db.password";
        String driverProp = "db.driver";
        String customizerProp = "db.customizer";
        String readMinProp = "db.read.min";
        String readMaxProp = "db.read.max";
        String writeMinProp = "db.write.min";
        String writeMaxProp = "db.write.max";
        String loggingProp = "db.logging";

        if (properties == null) {
            properties = new Properties();
            InputStream is = null;
            try {
                    String propertiesPath = System.getProperty("properties");
                    if (propertiesPath != null) {
                        log.info("Loading JPA properties from file: " + propertiesPath + PROPERTIES_FILENAME);
                        properties.load(new FileInputStream(new File(propertiesPath + PROPERTIES_FILENAME)));
                    } else {
                        log.info("Loading JPA properties from file: " + PROPERTIES_FILENAME);
                        is = getClass().getResourceAsStream(PROPERTIES_FILENAME);
                        properties.load(is);
                    }
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Properties error:" + e.getMessage(), e);
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (Exception ex) {
                }
            }
        }

        log.info("Using internal Eclipselink default connection pool");
        String[] nameValuePair = getNameValuePair(properties.getProperty(urlProp));
        props.put(nameValuePair[0], nameValuePair[1]);
        nameValuePair = getNameValuePair(properties.getProperty(driverProp));
        props.put(nameValuePair[0], nameValuePair[1]);
        nameValuePair = getNameValuePair(properties.getProperty(userProp));
        props.put(nameValuePair[0], nameValuePair[1]);
        nameValuePair = getNameValuePair(properties.getProperty(passwordProp));
        props.put(nameValuePair[0], nameValuePair[1]);
        nameValuePair = getNameValuePair(properties.getProperty(readMinProp));
        props.put(nameValuePair[0], nameValuePair[1]);
        nameValuePair = getNameValuePair(properties.getProperty(readMaxProp));
        props.put(nameValuePair[0], nameValuePair[1]);
        nameValuePair = getNameValuePair(properties.getProperty(writeMinProp));
        props.put(nameValuePair[0], nameValuePair[1]);
        nameValuePair = getNameValuePair(properties.getProperty(writeMaxProp));
        props.put(nameValuePair[0], nameValuePair[1]);
        nameValuePair = getNameValuePair(properties.getProperty(loggingProp));
        props.put(nameValuePair[0], nameValuePair[1]);
        nameValuePair = getNameValuePair(properties.getProperty(customizerProp));
        if (nameValuePair != null && nameValuePair.length > 1) {
            if (nameValuePair[1] != null && nameValuePair[1].length() > 0) {
                props.put(nameValuePair[0], nameValuePair[1]);
            }
        }
        log.debug("properties created successfully");
        return props;
    }

    private EntityManagerFactory getEmFactory() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, EntityManagerHelperHolder.INSTANCE.createProperties());
        }
        return emf;
    }

    public static EntityManager createEntityManager() {
        EntityManager em = EntityManagerHelperHolder.INSTANCE.getEmFactory().createEntityManager();
        // Keep alive query to trip the session customizer if necessary
        try {
            Query q = em.createNativeQuery("/* ping */");
            q.getSingleResult();
        } catch (Exception e) {
        }
        if (em == null) {
            throw new RuntimeException("The entity manager is null.");
        }
        return em;
    }

    public static void main(String[] args) {
        EntityManager em = EntityManagerHelperHolder.INSTANCE.createEntityManager();
        log.debug("Successfully created entity manager.");
    }

    public static void closeEntityManagerFactory() {
        if (EntityManagerHelperHolder.INSTANCE.emf != null) {
            try {
                EntityManagerHelperHolder.INSTANCE.emf.close();
                EntityManagerHelperHolder.INSTANCE.emf = null;
            } catch (Exception e) {
            }
        }
    }

    /**
     * Returns true if the entity manager has been initialized.
     */
    public static boolean isInitialized() {
        return EntityManagerHelperHolder.INSTANCE.emf != null;
    }

    public static Properties loadProperties() {
        if (properties == null) {
            EntityManagerHelperHolder.INSTANCE.createProperties();
        }
        return properties;
    }

    public static void initializeEntityManager(String filename) {
        log.info("Initializing ORM entity manager using properties file " + filename);
        if (filename == null) {
            throw new RuntimeException("Properties filename is null");
        }
        try {
            //initialize the orm/jpa db manager
            EntityManagerHelper.createEntityManager();
        } catch (Exception e) {
            log.error("Error initializing ORM entity manager: " + e.getMessage(), e);
            throw new RuntimeException("Error initializing ORM entity manager: " + e.getMessage(), e);
        }
        log.info("ORM entity manager intialization succeeded.");
    }
}
