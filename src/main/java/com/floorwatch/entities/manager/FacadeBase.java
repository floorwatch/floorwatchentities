package com.floorwatch.entities.manager;

import javax.persistence.EntityManager;

/**
 *
 * @author DaleDavis
 */
public class FacadeBase {

    public static final int DEFAULT_USER_ID = 1;

    /** Creates a new instance of FacadeBase */
    public FacadeBase() {
    }
    
    public EntityManager getEntityManager(){
        return EntityManagerHelper.createEntityManager();
    }
    
    protected void handleRuntimeException(String message, Exception e){
        throw new RuntimeException(message + ": " + e,e);
    }
    
}
