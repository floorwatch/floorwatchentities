package com.floorwatch.entities.manager;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.exceptions.DatabaseException;
import org.eclipse.persistence.exceptions.ExceptionHandler;
import org.eclipse.persistence.queries.ReadQuery;
import org.eclipse.persistence.sessions.Session;

public class FloorWatchSessionCustomizer implements SessionCustomizer {

    private Logger logger = Logger.getLogger(FloorWatchSessionCustomizer.class);

    public FloorWatchSessionCustomizer() {
    }

    public void customize(Session session) throws Exception {
        session.setExceptionHandler(new ExceptionHandler() {
            public Object handleException(RuntimeException exception) {
                logger.info("containsCommunicationException = "
                        + containsCommunicationsException(exception));
                if (exception instanceof DatabaseException) {
                    logger.info("DatabaseException = true");
                    DatabaseException de = (DatabaseException) exception;
                    if (containsCommunicationsException(de)) {
                        de.getAccessor().reestablishConnection(de.getSession());
                        if (de.getQuery() instanceof ReadQuery) {
                            return de.getSession().executeQuery(de.getQuery());
                        }
                    } else {
                        throw exception;
                    }
                } else {
                    throw exception;
                }
                return null;
            }
        });
    }

    private boolean containsCommunicationsException(Throwable throwable) {
        if (throwable instanceof CommunicationsException) {
            return true;
        }
        if (throwable.getCause() != null) {
            return containsCommunicationsException(throwable.getCause());
        }
        return false;
    }
}
