package com.floorwatch.entities.manager;

import com.floorwatch.entities.UserStores;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;

/**
 *
 * @author DaleDavis
 */
public class UserStoresFacade extends FacadeBase {

    private Logger log = Logger.getLogger(UserStoresFacade.class);

    /**
     * Creates a new instance of UserStoresFacade
     */
    public UserStoresFacade() {
    }

    public UserStores save(UserStores userStore) {
        EntityManager em = EntityManagerHelper.createEntityManager();
        try {
            em.getTransaction().begin();
            userStore = em.merge(userStore);
            em.getTransaction().commit();
            return userStore;
        } catch (Exception e) {
            try {
                em.getTransaction().rollback();
            } catch (Exception ex) {
            }
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch (Exception e) {
            }
        }
        return null;
    }

}
