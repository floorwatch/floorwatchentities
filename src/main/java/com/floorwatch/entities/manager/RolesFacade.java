package com.floorwatch.entities.manager;

import com.floorwatch.entities.Roles;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author DaleDavis
 */
public class RolesFacade extends FacadeBase {
    
    private Logger log = Logger.getLogger(RolesFacade.class);      
    
    /** Creates a new instance of RolesFacade */
    public RolesFacade() {
    }
    
    @SuppressWarnings("unchecked")
    public Roles findById(Integer id){
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Roles.findById");
            q.setParameter("id", id);
            return (Roles)q.getSingleResult();
        } catch(NoResultException nre) {
            throw nre;            
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    public Roles findByDescription(String description){
        EntityManager em = null;
        try {
            em = EntityManagerHelper.createEntityManager();
            Query q = em.createNamedQuery("Roles.findByDescription");
            q.setParameter("description", description);
            return (Roles)q.getSingleResult();
        } catch(NoResultException nre) {
            throw nre;            
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public List<Roles> findAll(){
        log.info("Facade Method: RolesFacade.findAll");
        EntityManager em = null;
        try {
            Calendar startCal = Calendar.getInstance();
            em = EntityManagerHelper.createEntityManager();
            List<Roles> list = new ArrayList<Roles>();
            Query q = em.createNamedQuery("Roles.findAll");
            List<Roles> results =  q.getResultList();
            Calendar endCal = Calendar.getInstance();
            log.info("Query Elasped Time: " + (endCal.getTimeInMillis() - startCal.getTimeInMillis()) + " ms");            
            if(results != null){
                list.addAll(results);
            }
            return list;
        } catch(Exception e) {
            this.handleRuntimeException(e.getMessage(), e);
        } finally {
            try {
                em.close();
            } catch(Exception e) {}
        }
        return null;
    }    
}
