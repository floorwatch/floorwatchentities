/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.floorwatch.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author daledavis
 */
@Entity
@Table(name = "networks")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Networks.findAll", query = "SELECT n FROM Networks n"),
    @NamedQuery(name = "Networks.findById", query = "SELECT n FROM Networks n WHERE n.id = :id"),
    @NamedQuery(name = "Networks.findByDescription", query = "SELECT n FROM Networks n WHERE n.description = :description"),
    @NamedQuery(name = "Networks.findByCreatedBy", query = "SELECT n FROM Networks n WHERE n.createdBy = :createdBy"),
    @NamedQuery(name = "Networks.findByCreatedAt", query = "SELECT n FROM Networks n WHERE n.createdAt = :createdAt"),
    @NamedQuery(name = "Networks.findByUpdatedBy", query = "SELECT n FROM Networks n WHERE n.updatedBy = :updatedBy"),
    @NamedQuery(name = "Networks.findByUpdatedAt", query = "SELECT n FROM Networks n WHERE n.updatedAt = :updatedAt"),
    @NamedQuery(name = "Networks.findByRowVersion", query = "SELECT n FROM Networks n WHERE n.rowVersion = :rowVersion")})
public class Networks implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "networkId")
    private List<UserNetworks> userNetworksList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "created_by")
    private int createdBy;
    @Basic(optional = false)
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @Column(name = "updated_by")
    private int updatedBy;
    @Basic(optional = false)
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Basic(optional = false)
    @Column(name = "row_version")
    private int rowVersion;

    public Networks() {
    }

    public Networks(Integer id) {
        this.id = id;
    }

    public Networks(Integer id, String description, int createdBy, Date createdAt, int updatedBy, Date updatedAt, int rowVersion) {
        this.id = id;
        this.description = description;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.updatedBy = updatedBy;
        this.updatedAt = updatedAt;
        this.rowVersion = rowVersion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(int rowVersion) {
        this.rowVersion = rowVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Networks)) {
            return false;
        }
        Networks other = (Networks) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.floorwatch.entities.Networks[ id=" + id + " ]";
    }

    @XmlTransient
    public List<UserNetworks> getUserNetworksList() {
        return userNetworksList;
    }

    public void setUserNetworksList(List<UserNetworks> userNetworksList) {
        this.userNetworksList = userNetworksList;
    }
    
}
