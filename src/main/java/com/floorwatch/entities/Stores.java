/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.floorwatch.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author daledavis
 */
@Entity
@Table(name = "stores")
@SqlResultSetMapping(name="StoresWithDistance", entities={@EntityResult(entityClass=Stores.class)}, columns={@ColumnResult(name="distance")})
 
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Stores.findAll", query = "SELECT s FROM Stores s"),
    @NamedQuery(name = "Stores.findById", query = "SELECT s FROM Stores s WHERE s.id = :id"),
    @NamedQuery(name = "Stores.findByDescription", query = "SELECT s FROM Stores s WHERE s.description = :description"),
    @NamedQuery(name = "Stores.findByAddress1", query = "SELECT s FROM Stores s WHERE s.address1 = :address1"),
    @NamedQuery(name = "Stores.findByAddress2", query = "SELECT s FROM Stores s WHERE s.address2 = :address2"),
    @NamedQuery(name = "Stores.findByCity", query = "SELECT s FROM Stores s WHERE s.city = :city"),
    @NamedQuery(name = "Stores.findByState", query = "SELECT s FROM Stores s WHERE s.state = :state"),
    @NamedQuery(name = "Stores.findByCountry", query = "SELECT s FROM Stores s WHERE s.country = :country"),
    @NamedQuery(name = "Stores.findByPostalCode", query = "SELECT s FROM Stores s WHERE s.postalCode = :postalCode"),
    @NamedQuery(name = "Stores.findByLatitude", query = "SELECT s FROM Stores s WHERE s.latitude = :latitude"),
    @NamedQuery(name = "Stores.findByLongitude", query = "SELECT s FROM Stores s WHERE s.longitude = :longitude"),
    @NamedQuery(name = "Stores.findByIsActive", query = "SELECT s FROM Stores s WHERE s.isActive = :isActive"),
    @NamedQuery(name = "Stores.findByCreatedBy", query = "SELECT s FROM Stores s WHERE s.createdBy = :createdBy"),
    @NamedQuery(name = "Stores.findByCreatedAt", query = "SELECT s FROM Stores s WHERE s.createdAt = :createdAt"),
    @NamedQuery(name = "Stores.findByUpdatedBy", query = "SELECT s FROM Stores s WHERE s.updatedBy = :updatedBy"),
    @NamedQuery(name = "Stores.findByUpdatedAt", query = "SELECT s FROM Stores s WHERE s.updatedAt = :updatedAt"),
    @NamedQuery(name = "Stores.findByRowVersion", query = "SELECT s FROM Stores s WHERE s.rowVersion = :rowVersion")})
public class Stores implements Serializable {
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Companies companyId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "storeId")
    private List<Flares> storeFlaresList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "storeId")
    private List<UserStores> userStoresList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "address_1")
    private String address1;
    @Column(name = "address_2")
    private String address2;
    @Basic(optional = false)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @Column(name = "state")
    private String state;
    @Basic(optional = false)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @Column(name = "postal_code")
    private String postalCode;
    @Column(name = "phone")
    private String phone;    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
    @Basic(optional = false)
    @Column(name = "is_active")
    private short isActive;
    @Basic(optional = false)
    @Column(name = "created_by")
    private int createdBy;
    @Basic(optional = false)
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @Column(name = "updated_by")
    private int updatedBy;
    @Basic(optional = false)
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Basic(optional = false)
    @Column(name = "row_version")
    private int rowVersion;
    @Transient
    Double distance;

    public Stores() {
    }

    public Stores(Integer id) {
        this.id = id;
    }

    public Stores(Integer id, String description, String address1, String city, String state, String country, String postalCode, short isActive, int createdBy, Date createdAt, int updatedBy, Date updatedAt, int rowVersion) {
        this.id = id;
        this.description = description;
        this.address1 = address1;
        this.city = city;
        this.state = state;
        this.country = country;
        this.postalCode = postalCode;
        this.isActive = isActive;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.updatedBy = updatedBy;
        this.updatedAt = updatedAt;
        this.rowVersion = rowVersion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }    

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public short getIsActive() {
        return isActive;
    }

    public void setIsActive(short isActive) {
        this.isActive = isActive;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(int rowVersion) {
        this.rowVersion = rowVersion;
    }
    
    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stores)) {
            return false;
        }
        Stores other = (Stores) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.floorwatch.entities.Stores[ id=" + id + " ]";
    }

    @XmlTransient
    public List<UserStores> getUserStoresList() {
        return userStoresList;
    }

    public void setUserStoresList(List<UserStores> userStoresList) {
        this.userStoresList = userStoresList;
    }

    @XmlTransient
    public List<Flares> getStoreFlaresList() {
        return storeFlaresList;
    }

    public void setStoreFlaresList(List<Flares> storeFlaresList) {
        this.storeFlaresList = storeFlaresList;
    }

    public Companies getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Companies companyId) {
        this.companyId = companyId;
    }
}
