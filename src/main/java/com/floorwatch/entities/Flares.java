/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.floorwatch.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author daledavis
 */
@Entity
@Table(name = "flares")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Flares.findAll", query = "SELECT f FROM Flares f"),
    @NamedQuery(name = "Flares.findById", query = "SELECT f FROM Flares f WHERE f.id = :id"),
    @NamedQuery(name = "Flares.findByCustomerText", query = "SELECT f FROM Flares f WHERE f.customerText = :customerText"),
    @NamedQuery(name = "Flares.findByManagerText", query = "SELECT f FROM Flares f WHERE f.managerText = :managerText"),
    @NamedQuery(name = "Flares.findByCustomerFollowupText", query = "SELECT f FROM Flares f WHERE f.customerFollowupText = :customerFollowupText"),
    @NamedQuery(name = "Flares.findByManagerFollowupText", query = "SELECT f FROM Flares f WHERE f.managerFollowupText = :managerFollowupText"),
    @NamedQuery(name = "Flares.findByResolved", query = "SELECT f FROM Flares f WHERE f.resolved = :resolved"),
    @NamedQuery(name = "Flares.findByResolvedAt", query = "SELECT f FROM Flares f WHERE f.resolvedAt = :resolvedAt"),
    @NamedQuery(name = "Flares.findByCreatedBy", query = "SELECT f FROM Flares f WHERE f.createdBy = :createdBy"),
    @NamedQuery(name = "Flares.findByCreatedAt", query = "SELECT f FROM Flares f WHERE f.createdAt = :createdAt"),
    @NamedQuery(name = "Flares.findByUpdatedBy", query = "SELECT f FROM Flares f WHERE f.updatedBy = :updatedBy"),
    @NamedQuery(name = "Flares.findByUpdatedAt", query = "SELECT f FROM Flares f WHERE f.updatedAt = :updatedAt"),
    @NamedQuery(name = "Flares.findByRowVersion", query = "SELECT f FROM Flares f WHERE f.rowVersion = :rowVersion")})
public class Flares implements Serializable {
    @Column(name = "resolved_stars")
    private Short resolvedStars;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "customer_text")
    private String customerText;
    @Column(name = "manager_text")
    private String managerText;
    @Column(name = "customer_followup_text")
    private String customerFollowupText;
    @Column(name = "manager_followup_text")
    private String managerFollowupText;
    @Basic(optional = false)
    @Column(name = "resolved")
    private short resolved;
    @Column(name = "resolved_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date resolvedAt;
    @Column(name = "resolved_text")
    private String resolvedText;    
    @Basic(optional = false)
    @Column(name = "created_by")
    private int createdBy;
    @Basic(optional = false)
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @Column(name = "updated_by")
    private int updatedBy;
    @Basic(optional = false)
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Basic(optional = false)
    @Column(name = "row_version")
    private int rowVersion;
    @JoinColumn(name = "manager_user_id", referencedColumnName = "id")
    @ManyToOne
    private Users managerUserId;
    @JoinColumn(name = "customer_user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users customerUserId;
    @JoinColumn(name = "store_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Stores storeId;

    public Flares() {
    }

    public Flares(Integer id) {
        this.id = id;
    }

    public Flares(Integer id, String customerText, short resolved, Date resolvedAt, int createdBy, Date createdAt, int updatedBy, Date updatedAt, int rowVersion) {
        this.id = id;
        this.customerText = customerText;
        this.resolved = resolved;
        this.resolvedAt = resolvedAt;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.updatedBy = updatedBy;
        this.updatedAt = updatedAt;
        this.rowVersion = rowVersion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerText() {
        return customerText;
    }

    public void setCustomerText(String customerText) {
        this.customerText = customerText;
    }

    public String getManagerText() {
        return managerText;
    }

    public void setManagerText(String managerText) {
        this.managerText = managerText;
    }

    public String getCustomerFollowupText() {
        return customerFollowupText;
    }

    public void setCustomerFollowupText(String customerFollowupText) {
        this.customerFollowupText = customerFollowupText;
    }

    public String getManagerFollowupText() {
        return managerFollowupText;
    }

    public void setManagerFollowupText(String managerFollowupText) {
        this.managerFollowupText = managerFollowupText;
    }

    public short getResolved() {
        return resolved;
    }

    public void setResolved(short resolved) {
        this.resolved = resolved;
    }

    public Date getResolvedAt() {
        return resolvedAt;
    }

    public void setResolvedAt(Date resolvedAt) {
        this.resolvedAt = resolvedAt;
    }
    
    public Short getResolvedStars() {
        return resolvedStars;
    }

    public void setResolvedStars(Short resolvedStars) {
        this.resolvedStars = resolvedStars;
    }

    public String getResolvedText() {
        return resolvedText;
    }

    public void setResolvedText(String resolvedText) {
        this.resolvedText = resolvedText;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(int rowVersion) {
        this.rowVersion = rowVersion;
    }

    public Users getManagerUserId() {
        return managerUserId;
    }

    public void setManagerUserId(Users managerUserId) {
        this.managerUserId = managerUserId;
    }

    public Users getCustomerUserId() {
        return customerUserId;
    }

    public void setCustomerUserId(Users customerUserId) {
        this.customerUserId = customerUserId;
    }

    public Stores getStoreId() {
        return storeId;
    }

    public void setStoreId(Stores storeId) {
        this.storeId = storeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Flares)) {
            return false;
        }
        Flares other = (Flares) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.floorwatch.entities.Flares[ id=" + id + " ]";
    }
}
