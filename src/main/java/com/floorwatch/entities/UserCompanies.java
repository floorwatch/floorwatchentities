/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.floorwatch.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author daledavis
 */
@Entity
@Table(name = "user_companies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserCompanies.findAll", query = "SELECT u FROM UserCompanies u"),
    @NamedQuery(name = "UserCompanies.findById", query = "SELECT u FROM UserCompanies u WHERE u.id = :id"),
    @NamedQuery(name = "UserCompanies.findByCreatedBy", query = "SELECT u FROM UserCompanies u WHERE u.createdBy = :createdBy"),
    @NamedQuery(name = "UserCompanies.findByCreatedAt", query = "SELECT u FROM UserCompanies u WHERE u.createdAt = :createdAt"),
    @NamedQuery(name = "UserCompanies.findByUpdatedBy", query = "SELECT u FROM UserCompanies u WHERE u.updatedBy = :updatedBy"),
    @NamedQuery(name = "UserCompanies.findByUpdatedAt", query = "SELECT u FROM UserCompanies u WHERE u.updatedAt = :updatedAt"),
    @NamedQuery(name = "UserCompanies.findByRowVersion", query = "SELECT u FROM UserCompanies u WHERE u.rowVersion = :rowVersion")})
public class UserCompanies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "created_by")
    private int createdBy;
    @Basic(optional = false)
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @Column(name = "updated_by")
    private int updatedBy;
    @Basic(optional = false)
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Basic(optional = false)
    @Column(name = "row_version")
    private int rowVersion;
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Companies companyId;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users userId;

    public UserCompanies() {
    }

    public UserCompanies(Integer id) {
        this.id = id;
    }

    public UserCompanies(Integer id, int createdBy, Date createdAt, int updatedBy, Date updatedAt, int rowVersion) {
        this.id = id;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.updatedBy = updatedBy;
        this.updatedAt = updatedAt;
        this.rowVersion = rowVersion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(int rowVersion) {
        this.rowVersion = rowVersion;
    }

    public Companies getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Companies companyId) {
        this.companyId = companyId;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserCompanies)) {
            return false;
        }
        UserCompanies other = (UserCompanies) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.floorwatch.entities.UserCompanies[ id=" + id + " ]";
    }
    
}
