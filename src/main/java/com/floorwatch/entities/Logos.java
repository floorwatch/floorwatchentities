/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.floorwatch.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author daledavis
 */
@Entity
@Table(name = "logos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Logos.findAll", query = "SELECT l FROM Logos l"),
    @NamedQuery(name = "Logos.findById", query = "SELECT l FROM Logos l WHERE l.id = :id"),
    @NamedQuery(name = "Logos.findByCreatedBy", query = "SELECT l FROM Logos l WHERE l.createdBy = :createdBy"),
    @NamedQuery(name = "Logos.findByCreatedAt", query = "SELECT l FROM Logos l WHERE l.createdAt = :createdAt"),
    @NamedQuery(name = "Logos.findByUpdatedBy", query = "SELECT l FROM Logos l WHERE l.updatedBy = :updatedBy"),
    @NamedQuery(name = "Logos.findByUpdatedAt", query = "SELECT l FROM Logos l WHERE l.updatedAt = :updatedAt"),
    @NamedQuery(name = "Logos.findByRowVersion", query = "SELECT l FROM Logos l WHERE l.rowVersion = :rowVersion")})
public class Logos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Lob
    @Column(name = "logo")
    private String logo;
    @Basic(optional = false)
    @Column(name = "created_by")
    private int createdBy;
    @Basic(optional = false)
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @Column(name = "updated_by")
    private int updatedBy;
    @Basic(optional = false)
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Basic(optional = false)
    @Column(name = "row_version")
    private int rowVersion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "logoId")
    private List<Companies> companiesList;

    public Logos() {
    }

    public Logos(Integer id) {
        this.id = id;
    }

    public Logos(Integer id, int createdBy, Date createdAt, int updatedBy, Date updatedAt, int rowVersion) {
        this.id = id;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.updatedBy = updatedBy;
        this.updatedAt = updatedAt;
        this.rowVersion = rowVersion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(int rowVersion) {
        this.rowVersion = rowVersion;
    }

    @XmlTransient
    public List<Companies> getCompaniesList() {
        return companiesList;
    }

    public void setCompaniesList(List<Companies> companiesList) {
        this.companiesList = companiesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Logos)) {
            return false;
        }
        Logos other = (Logos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.floorwatch.entities.Logos[ id=" + id + " ]";
    }
    
}
